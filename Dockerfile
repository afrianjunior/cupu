FROM node:alpine

WORKDIR /app
ADD . .

RUN yarn install

EXPOSE 3000

CMD ["node", "server.js"]